var navbarDiv = $('<nav class="navbar navbar-expand-sm bg-dark navbar-dark"></nav>');
$('body').prepend(navbarDiv);

function makeHeader(root=""){
  // Define navbar links
  var links = [];
  links.push({
      text:   "Resume",
      link: "resume/index.html"
  });
  links.push({
      text:   "Projects",
      link: "projects/index.html"
  });
  links.push({
      text:   "Uses",
      link: "uses/index.html"
  });
  links.push({
      text:   "Foods",
      link: "food/index.html"
  });
  if (root){
    root = root+'/';
  }
  for (var i in links) {
      navbarDiv.append('<a href='+root+links[i].link+'>'+links[i].text+'</a>')
  }
}
